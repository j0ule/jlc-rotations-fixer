<?php
    // Setup
    include_once ('./database_config.php');

    // Figure out what libraries are available to select
    $known_libraries_sql = mysqli_query($database_connection, 
        "SELECT DISTINCT `library` FROM `known_rotations` ORDER BY `library` ASC") 
        or die(mysqli_error($database_connection));
?>

<label for='parts_library'>
    Parts Library
</label>
<select id='parts_library' type='select'>
<?php
    // Iterate known libraries
    while($library_row = mysqli_fetch_assoc($known_libraries_sql)) {
        $library_name = $library_row['library'];
        echo 
        "<option value='$library_name'>$library_name</option>";
    }
?>
</select>