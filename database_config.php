<?php
  	// Include (well, "exclude") guard :-)
  	if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
		die("This file is meant to be used internally only");
	}

	// Don't go walking blind now
	ini_set('display_errors',1);
	error_reporting(E_ALL);

	// Change these to be for your database
	$dbhostname         = "localhost";
	$dbname             = "jlc_rotations_fixer";
	$dbusername         = "ilc_rotations_fixer";
	$dbpassword         = "jlc_pcb";

	if(!$database_connection = mysqli_connect($dbhostname, $dbusername, $dbpassword, $dbname)){
		die("Failed to connect to database");
	}
?>