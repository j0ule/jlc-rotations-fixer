<?php 
    // Setup
    include_once ('./database_config.php');
?>

<h2>Known rotations:</h2>
<div>
    <?php
        $known_rotations = mysqli_query($database_connection, 
            "SELECT * FROM `known_rotations` ORDER BY `footprint_name` DESC") 
            or die(mysqli_error($database_connection));
        
        echo "<table>";

        // Rotation table header
        echo 
            "<tr>".
                "<td>Library</td>".
                "<td>Footprint</td>".
                "<td>Rotation</td>".
                "<td>Actions</td>".
            "</tr>";

        // Add new rotation
        echo 
            "<tr id='new_rotation_entry'>".
                "<td>".
                    "<input type='text' id='new_database_library'/>".
                "</td>".
                "<td>".
                    "<input type='text' id='new_database_footprint'/>".
                "</td>".
                "<td>".
                    "<input type='text' id='new_database_rotation'/>".
                "</td>".
                "<td>".
                    "<input type='button' value='save' onclick='addNewRotation()'/>".
                "</td>".
            "</tr>";

        // Any rotations known?
        if(mysqli_num_rows($known_rotations) == 0) {
            echo "<tr><td>{None}</td></tr>";
        }

        else {

            // Iterate known rotations
            while($rotation_info = mysqli_fetch_assoc($known_rotations)) {

                $footprint_library = $rotation_info['library'];  
                $footprint_name = $rotation_info['footprint_name'];    
                $footprint_rotation = $rotation_info['rotation_value'];    

                echo 
                "<tr>".
                    "<td>$footprint_library</td>".
                    "<td>$footprint_name</td>".
                    "<td>$footprint_rotation</td>".
                    "<td>".
                        "<input type='button' value='delete' onclick=\"deleteRotation('$footprint_library', '$footprint_name')\"/>".
                        "<input type='button' value='edit' onclick=\"editRotation('$footprint_library', '$footprint_name', '$footprint_rotation')\"/>".
                    "</td>".
                "</tr>";
            }
        }

        echo "</table>"
    ?>
</div>

