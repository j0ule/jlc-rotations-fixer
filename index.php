<html>
    <head>
        <script src='./js/jquery-2.2.1.js' type='text/javascript'></script>
        <link rel='stylesheet' type='text/css' href='./styles.css'/>
        <title>JLC Rotations Fixer</title>
    </head>

    <body>
        <h1>JLC Rotations Fixer</h1>

        <div>
            <div id='library_loader'> 
                <?php include ('./library_loader.php'); ?>
            </div>        
            <div> 
                <label for='rotation_text'>
                    Title of "Rotation" Column
                </label>
                <input id='rotation_text' type='text' value='rotation'/>
            </div>
            <div>
                <label for='rotation_text'>
                    Title of "Footprint" Column
                </label>
                <input id='footprint_text' type='text' value='footprint'/>
            </div>
            <div>
                <label for='file_chooser'>
                    Upload your CPL file, as a .csv
                </label>
                <input onchange='fileChanged(event)' id='file_chooser' type='file' />
            </div>
        </div>

        <br><a href='' onclick='loadRotations(event)'>Edit rotations database</a><br>

        <div id='result_div'></div>
    </body>
</html>

<script type='text/javascript'>

    function loadRotations(event = null) {

        if(event) {
            event.preventDefault();
        }

        $('#result_div').load('./rotations.php', {
            // eee
        }, function() {
            // eee
        });

        $('#library_loader').load('./library_loader.php', {
            // eee
        }, function() {
            // eee
        });
    }

    function addNewRotation(){
        $('#result_div').load('./update_rotation.php', {
            footprint : $('#new_database_footprint').val(),
            library : $('#new_database_library').val(),
            rotation : $('#new_database_rotation').val(),
            action : 'add'
        }, 
        function() {
            loadRotations();
        });
    }

    function deleteRotation(library, footprint){
        $('#result_div').load('./update_rotation.php', {
            footprint : footprint,
            library : library,
            action : 'delete'
        }, 
        function() {
            loadRotations();
        });
    }

    function editRotation(library, footprint, rotation){
        $('#new_database_library').val(library);
        $('#new_database_footprint').val(footprint);
        $('#new_database_rotation').val(rotation);
        $('#new_database_rotation').focus();
    }

    function fileChanged(event) {

        let file = event.srcElement.files[0];
        let file_name = file.name;
        let reader = new FileReader();
        let file_64_encoded = null;

        reader.readAsBinaryString(file);

        reader.onload = function() {
            let btoaFile = btoa(reader.result);
            file_64_encoded = btoaFile.toString();
        };

        reader.onerror = function() {
            console.log('There are some problems with the file');
        };

        setTimeout(function(){
            $('#result_div').load('./parser.php', {
                file_64 : file_64_encoded,
                rotation_column : $('#rotation_text').val(),
                footprint_column : $('#footprint_text').val(),
                library : $('#parts_library').val(),
                file_name: file_name
            }, 
            function() {
                // Reset file picker
                $('#file_chooser').val('');

                // Get parsed file from hidden input
                let parsed_file = $('#parsed_file_64').val();

                // Convert the Base64 string back to text.
                let txt = atob(parsed_file);

                // Blob for saving.
                let blob = new Blob([txt], { type: "text/plain" });

                let url = URL.createObjectURL(blob)

                let cpl_download_link = $('#cpl_download_link')[0];
                
                cpl_download_link.href = url;
                cpl_download_link.download = `JLC_${file_name}`;
            });
        }, 100);
    };

</script>