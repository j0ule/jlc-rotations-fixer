<?php 
    // Setup
    include_once ('./database_config.php');

    // Get inputs
    $footprint = filter_input(INPUT_POST, 'footprint', FILTER_SANITIZE_STRING);
    $library = strtolower(filter_input(INPUT_POST, 'library', FILTER_SANITIZE_STRING));
    $rotation = filter_input(INPUT_POST, 'rotation', FILTER_SANITIZE_NUMBER_INT);
    $action =  filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);

    $rotation = intval(fmod(floatval($rotation), 360));

    // Delete a rotation
    if($action == 'delete' or $action == 'add') {

        // Delete old entry
        $delete_query = mysqli_query($database_connection, 
            "DELETE FROM `known_rotations` WHERE `footprint_name` = '$footprint' AND `library`='$library'")
            or die(mysqli_error($database_connection));
    }


    // Add a rotation
    if($action == 'add') {

        // No empty footprints
        if($footprint != '') {

            // Insert new entry
            $insert_query = mysqli_query($database_connection, 
                "INSERT INTO `known_rotations` SET `footprint_name` = '$footprint', `rotation_value`='$rotation', `library`='$library'")
                or die(mysqli_error($database_connection)); 
        }
    }

    // Done
    echo ucwords($action)." $footprint with $rotation rotation in $library";
?>
