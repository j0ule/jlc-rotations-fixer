<?php 
    // Setup
    include('./database_config.php');

    // Get input file
    $file_64 = $_POST['file_64'];
    $decoded_file = base64_decode($file_64);

    // Get column names
    $rotation_column_title = strtolower($_POST['rotation_column']);
    $footprint_column_title = strtolower($_POST['footprint_column']);

    // Get library
    $footprint_library = filter_input(INPUT_POST, 'library', FILTER_SANITIZE_STRING);

    // File name
    $file_name = filter_input(INPUT_POST, 'file_name', FILTER_SANITIZE_STRING);

    // Indicies of the columns
    $rotation_column_index = 0;
    $footprint_column_index = 0;
    $cpl_header_row_index = 0;

    // Parse CSV into associative array
    function parse_csv ($csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = true) {
        $enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string);
        $enc = preg_replace_callback(
            '/"(.*?)"/s',
            function ($field) {
                return urlencode(utf8_encode($field[1]));
            },
            $enc
        );
        $lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $enc);
        return array_map(
            function ($line) use ($delimiter, $trim_fields) {
                $fields = $trim_fields ? array_map('trim', explode($delimiter, $line)) : explode($delimiter, $line);
                return array_map(
                    function ($field) {
                        return str_replace('!!Q!!', '"', utf8_decode(urldecode($field)));
                    },
                    $fields
                );
            },
            $lines
        );
    }

    // Check if element is in array, case insenstivie
    function in_array_case_insensitive($needle, $haystack) {
        return in_array(strtolower($needle), array_map('strtolower', $haystack));
    }

    // Parse CSV header 
    function parse_cpl_header($columns) {

        echo "<tr>";

        global $rotation_column_title;
        global $footprint_column_title;
        global $rotation_column_index;
        global $footprint_column_index;
        
        foreach ($columns as $column_index => $column) {

            if(strtolower($column) == $rotation_column_title) {
                $column_text = "<span style='color:red'>$column [$column_index]</span>";
                $rotation_column_index = $column_index;
            }
            else if(strtolower($column) == $footprint_column_title) {
                $column_text = "<span style='color:red'>$column [$column_index]</span>"; 
                $footprint_column_index = $column_index;           
            }
            else {
                $column_text = "<span>$column</span>";
            }

            echo "<td class='header-element'>$column_text</td>";
        }

        echo "</tr>";
    }


    // Parse CSV row
    function parse_cpl_row($columns) {
        echo "<tr>";

        global $rotation_column_title;
        global $footprint_column_title;
        global $rotation_column_index;
        global $footprint_column_index;
        global $database_connection;
        global $footprint_library;

        // Holds information for this row
        $row_footprint = '';
        $row_rotation = '';
        $new_rotation = '';

        // First, iterate the columns until we find footprint, and rotation
        foreach ($columns as $column_index => $column) {
            if($column_index == $rotation_column_index) {
                $row_rotation = $column;
            }
            else if ($column_index == $footprint_column_index) {
                $row_footprint = filter_var($column, FILTER_SANITIZE_STRING);
            }
        }

        // Now see if we have a database entry for this
        $entry_query = mysqli_query($database_connection, 
            "SELECT * FROM `known_rotations` WHERE `library`='$footprint_library' AND `footprint_name` = '$row_footprint' LIMIT 1")
            or die(mysqli_error($database_connection));

        // Was there a match?
        if(mysqli_num_rows($entry_query) > 0) {
            $rotation_match = mysqli_fetch_assoc($entry_query);
            $new_rotation_modifier = floatval($rotation_match['rotation_value']);
        }

        // Now iterate again with (possible) changed info
        foreach ($columns as $column_index => $column) {
            
            // Black if no match
            $column_text = "<span>$column</span>";

            // Red if match
            if(isset($new_rotation_modifier)) {
                if($column_index == $footprint_column_index) {
                    // Footprint match
                    $column_text = "<span style='color:red'>$column</span>";
                }
                else if($column_index == $rotation_column_index) {

                    // Add modifer
                    $old_rotation = floatval($columns[$column_index]);
                    $new_rotation_value = fmod($old_rotation + $new_rotation_modifier, 360);

                    // Rotation match
                    $column_text = "<span style='color:red'>$new_rotation_value</span> ($column)";
                    
                    // Over-write old rotation in CSV array
                    $columns[$column_index] = $new_rotation_value;
                }
            }

            echo "<td class='row-element'>$column_text</td>";
        }

        echo "</tr>";

        return $columns;
    }

    // Parse the CSV file
    $cpl_array = parse_csv($decoded_file);
?>

<h2>Parsed CPL file ./<?php echo $file_name ?>:</h2>

<?php 

    // Now parse the CPL

    //echo "Searching for \"$rotation_column_title\"...<br>";

    echo "<table>";

    // Iterate through file and look for header
    $found_csv_header = false;
    foreach ($cpl_array as $line_number => $line) {

        if($found_csv_header == false) {
            $rotation_in_columns = in_array_case_insensitive($rotation_column_title, $line);
            if($rotation_in_columns) {
                
                $footprint_in_columns = in_array_case_insensitive($footprint_column_title, $line);
                
                if($footprint_in_columns) {
                    // Found header! Parse header...
                    $found_csv_header = true;
                    $cpl_header_row_index = $line_number;
                    parse_cpl_header($line);
                }

                continue;
            }
            else {
                //echo ("<tr><td>Header not found, skipping line $row...</td></tr>");
                //$cpl_array[$row] = '';
                continue;
            }
        }

        // If we got here the header has been found, start parsing rows
        $cpl_array[$line_number] = parse_cpl_row($line);
    }

    echo "</table>";

    // We now have a modified CPL we must turn back into a CSV file
    $new_cpl_csv = '';

    // Prepend header
    for($i = 0; $i < $cpl_header_row_index; $i++) {
        $new_cpl_csv .= $cpl_array[$i][0] . "\n";
    }

    // Generate new CPL entries
    for($i = $cpl_header_row_index; $i < count($cpl_array); $i++) {
        $new_cpl_line = '';
        foreach ($cpl_array[$i] as $cpl_column_index => $cpl_column) {
            if(is_numeric($cpl_column)) {
                $new_cpl_line .= "$cpl_column,";
            }
            else {
                $new_cpl_line .= "\"$cpl_column\",";
            }
        }
        $new_cpl_line = trim($new_cpl_line, ',');
        $new_cpl_line .= "\n";
        $new_cpl_csv  .= $new_cpl_line;
    }

    $new_bytes = base64_encode($new_cpl_csv);

    echo "<input type='hidden' value='$new_bytes' id='parsed_file_64'/>";
?>

<h2>Download <a id='cpl_download_link' target='_self'>JLC_<?php echo $file_name?></a></h2>